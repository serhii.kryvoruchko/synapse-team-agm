import {Component, OnInit} from '@angular/core';
import {ObjectInterface} from "../types/object.interface";
import {BehaviorSubject} from "rxjs";
import {MarkersService} from "../service/markers.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  markers!: ObjectInterface[]
  defaultZoom: number = 8
  search = ''
  activeItem!: ObjectInterface
  latitudeCar = new BehaviorSubject<any>(51.678418)
  longitudeCar = new BehaviorSubject<any>(7.809007)
  zoom: BehaviorSubject<number> = new BehaviorSubject<number>(this.defaultZoom);

  constructor(private markersService: MarkersService) {
  }

  ngOnInit() {
    this.getMarkers()
  }

  getMarkers() {
    this.markers = this.markersService.getAll()
  }

  choiceCar(m: ObjectInterface) {
    if (this.markers.filter(mar => mar.id === m.id)) {
      this.latitudeCar.next(m.latitude)
      this.longitudeCar.next(m.longitude)
      this.zoom.next(this.defaultZoom)
      console.log(this.latitudeCar)
    }
  }

  onSelect(m: ObjectInterface) {
    this.activeItem = m
  }

  clickMap(e: any) {
    // console.log('mapClick: ', e)//???
  }

  markerClick(e: any) {
    // console.log('markerClick: ', e)
  }
}
