import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppComponent} from './app.component';
import {FormsModule} from "@angular/forms";
import {AgmCoreModule} from "@agm/core";
import {SearchCarPipe} from "../pipe/searchCar.pipe";

@NgModule({
  declarations: [
    AppComponent,
    SearchCarPipe
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAMJJ6E46cmGsCKtk5Q81DTb432_DO5GOA'
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
