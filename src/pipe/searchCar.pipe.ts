import {Pipe, PipeTransform} from '@angular/core';
import {ObjectInterface} from "../types/object.interface";

@Pipe({
  name: 'searchCar'
})

export class SearchCarPipe implements PipeTransform{
  transform(markers: ObjectInterface[], search =''): any {
    if(!search.trim()) {
      return markers
    }

    return markers.filter(marker => {
      return marker.name.toLowerCase().includes(search.toLowerCase())
    })
  }

}
