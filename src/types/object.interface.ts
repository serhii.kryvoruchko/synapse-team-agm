export interface ObjectInterface {
  id: number
  latitude: number
  longitude: number
  name: string
}
